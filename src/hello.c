#include "hello.h"

#include <stdio.h>

void hello(){
    #ifdef NDEBUG
    printf("Hello World Release!\n");
    #else
    printf("Hello World Debug!\n");
    #endif
}
