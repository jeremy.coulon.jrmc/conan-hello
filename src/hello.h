#pragma once

#ifdef WIN32
  #define HELLO_EXPORT __declspec(dllexport)
#else
  #define HELLO_EXPORT
#endif

#ifdef __cplusplus
extern "C" {
#endif

HELLO_EXPORT void hello();

#ifdef __cplusplus
}
#endif
